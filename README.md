# My Parenting Tips Diary
## Samedi 02 janvier 2021 03h21
### Simuler une classe à la maison
Je crois qu'il est important de bâtir une relation avec l'enfant en créant un genre d'habitude.
Par exemple, pour le préparer pour l'école, je pense acheter un tableau blanc, un pupitre et tout le reste
de sorte à simuler un genre de petit classe à la maison.
Ainsi, la transition de l'enfant vers l'école sera très facile.
### Apprendre la science par des objets
Ce que je remarque dans le système d'éducation actuel, les étudiants apprennent la science « sur papier ».
Oui, il arrive que dans certains cours, l'école ont certains instruments (chimie, physique).
Mais, je crois qu'avoir ces mêmes instruments à la maison facilitera grandement l'apprentissage de l'enfant.
Surtout, lorsque ce n'est pas tous les enfants qui apprends à la même vitesse.

Ou encore « Apprendre la science par projets »
Hyperliens utiles:
* https://mommypoppins.com/kids/50-easy-science-experiments-for-kids-fun-educational-activities-using-household-stuff
* https://www.sciencefun.org/kidszone/experiments/
* https://sciencebob.com/category/experiments/

### Remarque(s)
Je dois admettre que ce sont des choses que j'aurai tellement aimé avoir à la maison,
mais mes parents n'avaient pas nécessairement les moyens financiers...
Donc, il est normal que lorsque je serai un parent à mon tour, je veux le meilleur pour mes enfants.

# Dimanche 03 janvier 2021 12h55
## Le contact humain
Après la naissance de l'enfant, je crois qu'il est important d'avoir le contact humain.
Utiliser le câlin (hug) est un moyen de démontrer à un enfant qu'on l'aime.
Même si lorsqu'il/elle deviendra un ado, il y aura toujours ce lien qui unit l'enfant à ses parents.
## Les types d'intelligence
Il est impossible de prédire quelle profession l'enfant excellera.
Cependant, on peut mettre toutes les chances en bien lui outiller.
Voir « Gardner's Theory of Multiple Intelligences »
* Visual-Spatial
* Linguistic-Verbal
* Logical-Mathematical
* Bodily-Kinesthetic
* Musical
* Interpersonal
* Intrapersonal
* Naturalistic

# Mardi 05 janvier 2021 14h16
## Storytelling
Pour développer l'imagination de l'enfant, je crois qu'il est important d'introduire cette tradition de lire des histoires.
De plus, cela m'aidera aussi, le parent, à mieux comprendre la structure d'une bonne histoire.
## L'art de la musique
Je veux que mes enfants développent un talent pour les actions liées à la musique:
* Chanter
* Danser
* Jouer un instrument

# Jeudi 07 janvier 2021 14h00
## Food Group
Je veux donner tous les nutriments à mes enfants en respectant les groupes alimentaires.
1. Les produits laitiers
2. Les légumes
3. Les fruits
4. Les grains céréaliers
5. Les protéines

# Samedi 08 janvier 2021 03h49
## MBTI
Pour mieux comprendre son enfant, il est très utile de connaître son personnalité type:

![MBTI](images/16-personality-types-infographic.jpg)

# Samedi 16 janvier 2021 15h47
## Enseigner pour apprendre
Pour appliquer l'idéologie suivante:

![Confucius](images/confucius_quote_01.png)

Je veux m'impliquer dans les études de mes futurs enfants en ayant un tableau blanc à la maison.
Je veux m'impliquer dans leur devoir.
Pourquoi ? Je suis d'avis que la meilleure façon de maîtriser un sujet est de l'enseigner à un autre.
Donc, une personne qui connaît bien sa matière est capable de le vulgariser.

# Samedi 23 janvier 2021 07h28
## If you do not use it, you will lose it

![Confucius](images/john-templeton.jpg)

C'est la raison pourquoi on oublie lorsqu'on laisse quelque chose de côté pour très longtemps.

# Dimanche 31 janvier 2021 15h35

« Il vaut mieux prévenir que guérir » 
Selon-moi, il faut savoir reconnaître ses limites. Le corps humain peut s'endommager et cela prends du temps pour se rétablir.

![VautMieuxPrevenirQueGuerir](images/VautMieuxPrevenirQueGuerir.jpeg "VautMieuxPrevenirQueGuerir")

« Il faut battre le fer pendant qu'il est chaud »
Selon-moi, il ne faut pas laisser une opportunité passée, car elle se présente occasionnellement.

![StrikeTillTheIronIsHow](images/StrikeTillTheIronIsHow.jpg "StrikeTillTheIronIsHow")

« Il ne faut pas mettre tous ses œufs dans le même panier. »

![DontPutAllYourEggsInOneBasket](images/DontPutAllYourEggsInOneBasket.png "DontPutAllYourEggsInOneBasket")

« Le monde appartient à celui qui se lève tôt. »

![TheEarlyBirdCatchesTheWorm](images/TheEarlyBirdCatchesTheWorm.png "TheEarlyBirdCatchesTheWorm")

« Il ne faut pas vendre la peau de l'ours avant de l'avoir tué. »

![DontCountYourChickensBeforeTheyreHatched](images/DontCountYourChickensBeforeTheyreHatched.jpg "DontCountYourChickensBeforeTheyreHatched")

« Petit à petit, l'oiseau fait son nid. »

![PetitAPetitOiseauFaitSonNid](images/PetitAPetitOiseauFaitSonNid.jpg "PetitAPetitOiseauFaitSonNid")

« When the cat's away the mice will play. »

![WhenTheCatAwayTheMiceWillPlay](images/WhenTheCatAwayTheMiceWillPlay.jpg "WhenTheCatAwayTheMiceWillPlay")

« Birds of a feather flock together. »

![BirdsOfAFeatherFlockTogether](images/BirdsOfAFeatherFlockTogether.jpeg "BirdsOfAFeatherFlockTogether")

« Qui vole un œuf vole un bœuf. »

![QuiVoleUnOeufVoleUnBoeuf](images/QuiVoleUnOeufVoleUnBoeuf.png "QuiVoleUnOeufVoleUnBoeuf")

« Un tiens vaut mieux que deux tu l'auras. »

![ABirdInTheHandWorthTwoInTheBush](images/ABirdInTheHandWorthTwoInTheBush.jpg "ABirdInTheHandWorthTwoInTheBush")

« Never put off till tomorrow what you can do today. »

![NeverPutOffTillTomorrowWhatYouCanDoToday](images/NeverPutOffTillTomorrowWhatYouCanDoToday.jpg "NeverPutOffTillTomorrowWhatYouCanDoToday")

« All's fair in love and war. »

![AllFairInLoveAndWar](images/AllFairInLoveAndWar.jpg "AllFairInLoveAndWar")

## Dimanche 14 Février 2021 17h24
Il y a longtemps que je voulais ajouter ceci :

![FoodGroup](images/FoodGroup.jpg "FoodGroup")

**Réflexion**
Dans mon temps, lorsque j'étais au primaire, il me semble que la concept des groups alimentaires était enseigné à l'école.

## Nutrition = Macronutrient + Micronutrient
Pour plus d'information, voir:

https://mynutrition.wsu.edu/nutrition-basics

Le site ci-haut semble faire un bon résumé.

## Le système de santé public au Canada - Une question d'identité
1. Une personne riche (millionnaire, milliardaire) peut un jour tomber dans la pauvreté.
2. Une personne pauvre peut un jour atteindre la richesse.

3. Une personne, riche ou pauvre, recevra le même niveau de traitement au Canada.
En d'autres mots, les médecins font de leur mieux pour guérir leur patient, peu importe leur statut social,
car nous croyons que toute être humain sont créé égaux. Ou encore, le malheur peut arriver à n'importe qui.
LA plupart du temps, une personne n'a besoin qu'un coup de main pour se reprendre en main dans la société.
   
Le résultat:
Au Canada, les gens sont plus généreuses parce qu'ils ont reçu de l'aide lorsqu'ils en avaient le plus de besoin (dans les bas moments de leur vie).
Aux États-Unis, c'est chacun pour soi. C'est pourquoi, une personne pauvre qui devienne riche agit de façon égoiste:
Personne ne lui a donné un coup de main lorsqu'il en avait de besoin, alors pourquoi aider les autres ?

## Lundi 22 Février 2021 07h44

![LogicalFallacies](images/LogicalFallacies.jpg "LogicalFallacies")

Les différentes catégories/divisions de la science:

![BranchOfScience](images/BranchOfScience.png "BranchOfScience")

# Vendredi 19 Mars 2021 09h36
Par l'auteur du livre "Start With Why" - Simon Sinek:

![TheGoldenCircle](images/TheGoldenCircle.jpg "TheGoldenCircle")

Par l'auteur du livre "Rich Dad's Cashflow Quadrant: Guide to Financial Freedom" - Robert Kiyosaki:

![TheCashflowQuadrant](images/TheCashflowQuadrant.jpg "TheCashflowQuadrant")

# Jeudi 06 mai 2021 19h16
The Crab Mentality

        Crab mentality, also known as crab theory, crabs in a bucket (also barrel, basket, or pot) mentality, or the crab-bucket effect, 
        is a way of thinking best described by the phrase "if I can't have it, neither can you".
        The metaphor is derived from a pattern of behavior noted in crabs when they are trapped in a bucket.
        While any one crab could easily escape, its efforts will be undermined by others, ensuring the group's collective demise.
        The analogy in human behavior is claimed to be that members of a group will attempt to reduce the self-confidence of any member who achieves success beyond the others, 
        out of envy, resentment, spite, conspiracy, or competitive feelings, to halt their progress.

Video: https://www.youtube.com/watch?v=tv6X0vFu_uI

# Mercredi 12 Mai 2021 18h51
![LeaveThePastInThePast01](images/LeaveThePastInThePast01.jpg)

![LeaveThePastInThePast02](images/LeaveThePastInThePast02.jpg)

Beaucoup de pays, de familles possède trop de fierté où ils veulent que les gens les traite comme c'était dans leur âge d'or:
Par exemple, comme s'ils sont de la royauté, des riches, etc.
Tous empires finissent par tomber. Et j'ajouterais, que les gens « Move-on ».
Donc, il faut apprendre à laisser la passé, au passé.

# Jeudi 13 mai 2021 19h00

    The law of the instrument, law of the hammer, Maslow's hammer (or gavel), or golden hammer is a cognitive bias that involves an over-reliance on a familiar tool. 
    As Abraham Maslow said in 1966, "I suppose it is tempting, if the only tool you have is a hammer, to treat everything as if it were a nail.

source: https://en.wikipedia.org/wiki/Law_of_the_instrument

# Mercredi 09 juin 2021 20h49
![TimeMoneyEnergy](images/TimeMoneyEnergy.png)

# Vendredi 30 juillet 2021 14h23:

![ResilienceFramework](images/ResilienceFramework.png)

Source: https://www.boingboing.org.uk/use-resilience-framework-academic-resilience/

# Mardi 03 août 2021 09h27

![DimentionOfServiceManagement](images/DimentionOfServiceManagement.jpg)

Source: https://www.bmc.com/blogs/people-process-technology/

# Jeudi 05 août 2021 17h09

"Apprendre et retenir" ou "Mémoriser et retenir" n'est pas la même chose !

![Confucius_quote_02](images/Confucius_quote_02.jpeg)

![BenjaminFranklin_quote_01](images/BenjaminFranklin_quote_01.jpg)

# Dimanche 05 septembre 2021 21h46

![NapoleonBonaparteQuote_01](images/NapoleonBonaparteQuote_01.jpg)

![NapoleonBonaparteQuote_02](images/NapoleonBonaparteQuote_02.jpg)

![AlexanderTheGreatQuote_01](images/AlexanderTheGreatQuote_01.jpg)

# Jeudi 04 Novembre 2021 22h28

![KarlMarxQuote01](images/KarlMarxQuote01.jpg)

# Mercredi 17 Novembre 2021 23h53

![HardWorkBeatTalent](images/HardWorkBeatTalent.jpg)

# Dimanche 21 Novembre 2021 18h11

![ByTeachingOthersYouWillLearnYourself](images/ByTeachingOthersYouWillLearnYourself.jpg)

# Mercredi 08 Décembre 2021 09h02

![BloomsTaxonomy](images/BloomsTaxonomy.jpg)

For more info, visit:
https://cft.vanderbilt.edu/guides-sub-pages/blooms-taxonomy/

# Mercredi 08 Décembre 2021 10h25
Méditation et les muscles du corps humain:

* Skeletal Muscle
* Smooth Muscle
* Cardiac Muscle

Sources:
* https://training.seer.cancer.gov/anatomy/muscular/types.html
* https://www.javatpoint.com/voluntary-muscles-vs-involuntary-muscles

# Jeudi 23 Décembre 2021 14h13
![LaZoneDeConfort](images/LaZoneDeConfort.jfif)

# Vendredi 25 Mars 2022 15h48
## Barnum Effect

> Barnum Effect, also called Forer Effect, in psychology, the phenomenon that occurs when individuals believe that personality descriptions apply specifically to them (more so than to other people), despite the fact that the description is actually filled with information that applies to everyone. The effect means that people are gullible because they think the information is about them only when in fact the information is generic. The Barnum Effect came from the phrase often attributed (perhaps falsely) to showman P. T. Barnum that a “sucker” is born every minute. Psychics, horoscopes, magicians, palm readers, and crystal ball gazers make use of the Barnum Effect when they convince people that their description of them is highly special and unique and could never apply to anyone else.

Source: https://www.britannica.com/science/Barnum-Effect

## Suspension Bridge Effect

> The suspension bridge effect happens when a person crosses a suspension bridge and he sees someone of the opposite sex. his fear of falling down causes his heart to pound. He then mistakes that for the heart-pounding feeling felt when falling in love with the opposite sex.

Source: https://www.urbandictionary.com/define.php?term=Suspension%20Bridge%20Effect

## Stockholm Syndrome 

> Stockholm syndrome is a coping mechanism to a captive or abusive situation. People develop positive feelings toward their captors or abusers over time. This condition applies to situations including child abuse, coach-athlete abuse, relationship abuse and sex trafficking. Treatment includes psychotherapy (“talk therapy”) and medications if needed.

Source: https://my.clevelandclinic.org/health/diseases/22387-stockholm-syndrome

# Samedi 26 Mars 2022 11h47
## Maslow Pyramid of Needs
> It is important to note that Maslow's (1943, 1954) five-stage model has been expanded to include cognitive and aesthetic needs (Maslow, 1970a) and later transcendence needs (Maslow, 1970b).

> Changes to the original five-stage model are highlighted and include a seven-stage model and an eight-stage model; both developed during the 1960s and 1970s.

1. Biological and physiological needs - air, food, drink, shelter, warmth, sex, sleep, etc.

2. Safety needs - protection from elements, security, order, law, stability, freedom from fear.

3. Love and belongingness needs - friendship, intimacy, trust, and acceptance, receiving and giving affection and love. Affiliating, being part of a group (family, friends, work).

4. Esteem needs - which Maslow classified into two categories: (i) esteem for oneself (dignity, achievement, mastery, independence) and (ii) the need to be accepted and valued by others (e.g., status, prestige).

5. Cognitive needs - knowledge and understanding, curiosity, exploration, need for meaning and predictability.

6. Aesthetic needs - appreciation and search for beauty, balance, form, etc.

7. Self-actualization needs - realizing personal potential, self-fulfillment, seeking personal growth and peak experiences. A desire “to become everything one is capable of becoming”(Maslow, 1987, p. 64).

8. Transcendence needs - A person is motivated by values which transcend beyond the personal self (e.g., mystical experiences and certain experiences with nature, aesthetic experiences, sexual experiences, service to others, the pursuit of science, religious faith, etc.).

Source: https://www.simplypsychology.org/maslow.html